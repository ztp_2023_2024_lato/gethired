import Cookies from 'js-cookie';
import React from 'react'
import { Outlet, Navigate } from 'react-router-dom'



export default function PrivateRoutes() {
    let  token = Cookies.get('accessToken') == null ? false : true;
    return (
        <>
            {token ? <Outlet  /> : <Navigate to="/login" />};
        </>

    )
}