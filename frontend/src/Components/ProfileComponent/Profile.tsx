import React, {ChangeEventHandler, useEffect, useState} from 'react';
import './Profile.css';
import {jwtDecode} from "jwt-decode";
import Cookies from "universal-cookie";
import {useNavigate} from "react-router-dom";
import axios from "axios";
import TextField from '@mui/material/TextField';
import {createTheme, Theme, useTheme} from "@mui/material/styles";
import {outlinedInputClasses, ThemeProvider} from "@mui/material";
import Button from "@mui/material/Button";

const customTheme = (outerTheme: Theme) =>
    createTheme({
        palette: {
            mode: outerTheme.palette.mode,
        },
        components: {
            MuiTextField: {
                styleOverrides: {
                    root: {
                        '--TextField-brandBorderColor': '#E0E3E7',
                        '--TextField-brandBorderHoverColor': '#B2BAC2',
                        '--TextField-brandBorderFocusedColor': '#e3e5e7',
                        '& label.Mui-focused': {
                            color: 'var(--TextField-brandBorderFocusedColor)',
                        },
                    },
                },
            },
            MuiOutlinedInput: {
                styleOverrides: {
                    notchedOutline: {
                        borderColor: 'var(--TextField-brandBorderColor)',
                    },
                    root: {
                        [`&:hover .${outlinedInputClasses.notchedOutline}`]: {
                            borderColor: 'var(--TextField-brandBorderHoverColor)',
                        },
                        [`&.Mui-focused .${outlinedInputClasses.notchedOutline}`]: {
                            borderColor: 'var(--TextField-brandBorderFocusedColor)',
                        },
                        [`.${outlinedInputClasses.notchedOutline}`]: {
                            borderColor: 'var(--TextField-brandBorderHoverColor)',
                        },
                    },
                },
            },
            MuiFilledInput: {
                styleOverrides: {
                    root: {
                        '&::before, &::after': {
                            borderBottom: '2px solid var(--TextField-brandBorderColor)',
                        },
                        '&:hover:not(.Mui-disabled, .Mui-error):before': {
                            borderBottom: '2px solid var(--TextField-brandBorderHoverColor)',
                        },
                        '&.Mui-focused:after': {
                            borderBottom: '2px solid var(--TextField-brandBorderFocusedColor)',
                        },
                    },
                },
            },
            MuiInput: {
                styleOverrides: {
                    root: {
                        '&::before': {
                            borderBottom: '2px solid var(--TextField-brandBorderColor)',
                        },
                        '&:hover:not(.Mui-disabled, .Mui-error):before': {
                            borderBottom: '2px solid var(--TextField-brandBorderHoverColor)',
                        },
                        '&.Mui-focused:after': {
                            borderBottom: '2px solid var(--TextField-brandBorderFocusedColor)',
                        },
                    },
                },
            },
        },
    });

const Profile: React.FC = () => {
    const cookies = new Cookies();
    const navigate = useNavigate();
    const [actualUsername, setActualUsername] = useState('');
    const outerTheme = useTheme();
    const [newUsername, setNewUsername] = useState('')

    useEffect(() => {
        const token = cookies.get('bearer');
        if (token) {
            const decodedToken = jwtDecode(token);
            axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/users/${decodedToken.sub}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            }).then(response => {
                setActualUsername(response.data.username);
            }).catch(error => {
                console.log(error);
            });
        } else {
            navigate('/login');
        }
    }, []);

    function changeUsername() {
        axios.put(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/users/${actualUsername}`, {
                username: newUsername
            },
            {
                headers: {
                    Authorization: `Bearer ${cookies.get('bearer')}`
                }

            }
        ).then(response => {
            cookies.remove('bearer');
            navigate('/login');
        }).catch(error => {
            console.log(error);
        });
    }

    function handleUsernameChange(e: React.ChangeEvent<HTMLInputElement>) {
        setNewUsername(e.target.value);
    }

    return (
        <div className={"main-container"}>
            <div className="actual-username">
                Actual username: {actualUsername}
            </div>

            <div className="form-username-change">
                Wanna change your username?
                <ThemeProvider theme={customTheme(outerTheme)}>
                    <TextField id="filled-basic" label="New username" variant="filled" onChange={handleUsernameChange}/>
                </ThemeProvider>
                <Button variant="contained" onClick={changeUsername}>Submit</Button>
            </div>
        </div>
    );
}
export default Profile;