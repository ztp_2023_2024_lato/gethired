import React, {useEffect, useState} from 'react';
import 'dayjs/locale/en-gb';
import "./Vacancies.css";
import dayjs from "dayjs";
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import {Modal, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import axios from "axios";
import Cookies from "universal-cookie";
import {Button} from "reactstrap";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import {jwtDecode} from "jwt-decode";

dayjs.extend(utc);
dayjs.extend(timezone);

function createData(
    name: string,
    description: string,
    companySize: number,
) {
    return {name, description, companySize};
}

const rows = [
    createData('Full-stack developer', "Microsoft", 100000),
    createData('Tester of gaming product', "Comarch", 13294),
    createData('Senior Go developer', "Samsung", 51932),
    createData('PHP Developer', "Accenture", 13294),
    createData('Product Manager', "Unity-T", 13),
];

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'gray',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    color: 'white'
};

const Vacancies: React.FC = () => {

        const [vacanciesList, setVacanciesList] = useState([]);
        const cookies = new Cookies();
        const [open, setOpen] = useState(false);
        const [coverLetterText, setCoverLetterText] = useState('');
        const [selectedVacancyId, setSelectedVacancyId] = useState('');

        const handleChangeText = (e: any) => {
            setCoverLetterText(e.target.value);
        }

        const handleClose = () => setOpen(false);

        useEffect(() => {
            const token = cookies.get('bearer');
            axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/vacancy`, {
                headers: {Authorization: `Bearer ${token}`}
            })
                .then((response) => {
                    setVacanciesList(response.data);
                })
        }, []);

        const handleCreate = () => {
            // console.log(companyName, companySize, companyDescription);
            // if (companyName === '' || isNaN(companySize) || companyDescription === '') {
            //     setErrorText('Fill all fields correctly!')
            //     return
            // }
            //
            const token = cookies.get('bearer');
            const decodedToken = jwtDecode(token);

            axios.post(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/application`, {
                userId: decodedToken.sub,
                vacancyId: selectedVacancyId,
                text: coverLetterText,
            }, {
                headers: {Authorization: `Bearer ${cookies.get('bearer')}`}
            })
                .then((response) => {
                    console.log(response)
                    setOpen(false)
                })
        };

        const handleApply = (e: any) => {
            setOpen(true);
            setCoverLetterText('');
            setSelectedVacancyId(e.target.id);
            console.log(e.target.id);
        }


        return (
            <>
                <div className="vacancies-page">
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                Create new company
                            </Typography>
                            <div className="form">
                                <TextField id="standard-basic" label="Cover letter (text)" variant="standard" inputProps={{
                                    style: {color: 'white', margin: '0 auto'},
                                }} onChange={handleChangeText}/>
                                {/*<div className="error-text">{errorText}</div>*/}
                                <Button onClick={handleCreate}>Create</Button>
                            </div>
                        </Box>
                    </Modal>

                    <TableContainer component={Paper} style={{height: 100 + '%'}}>
                        <Table sx={{minWidth: 650}} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Title</TableCell>
                                    <TableCell align="right">Description</TableCell>
                                    <TableCell align="right">Company Name</TableCell>
                                    <TableCell align="right">Apply</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {vacanciesList.map((vacancy: any) => (
                                    <TableRow
                                        key={vacancy.id}
                                        sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                    >
                                        <TableCell component="th" scope="row">
                                            {vacancy.title}
                                        </TableCell>
                                        <TableCell align="right">{vacancy.description}</TableCell>
                                        <TableCell align="right">{vacancy.companyName}</TableCell>
                                        <TableCell align="right">
                                            <Button onClick={handleApply} id={vacancy.id}>Apply</Button>
                                        </TableCell>

                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>

            </>
        );
    }
;

export default Vacancies;
