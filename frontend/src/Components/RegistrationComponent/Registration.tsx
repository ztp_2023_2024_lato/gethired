import React, {useState} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Alert, Grid, Snackbar, TextField} from "@mui/material";
import {DemoContainer} from '@mui/x-date-pickers/internals/demo';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {DatePicker} from '@mui/x-date-pickers/DatePicker';
import 'dayjs/locale/en-gb';
import "./Registration.css";
import dayjs from "dayjs";
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import useMediaQuery from "@mui/material/useMediaQuery";
import Cookies from 'universal-cookie';
import regex from "../../support-functions/regex";
import {useNavigate} from 'react-router-dom';
import {jwtDecode} from "jwt-decode";
import {JwtPayload} from "jwt-decode";
import {useAuth} from "../../support-functions/auth-provider";

dayjs.extend(utc);
dayjs.extend(timezone);


const Registration: React.FC<{ user: JwtPayload, setUser: any }> = ({user, setUser}) => {
    const matchesBig = useMediaQuery('(min-width:600px)');
    const Auth = useAuth();
    const cookies = new Cookies();

    const [formData, setFormData] = useState({
        username: '',
        password: '',
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        setFormData((prevData) => ({...prevData, [name]: value}))
    }

    const navigate = useNavigate();

    const [errorMessage, setErrorMessage] = useState("");

    const handleSubmit = (e: React.ChangeEvent<any>) => {

        e.preventDefault();

        setFormData((prevData) => ({...prevData}));

        axios
            .post(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/auth/register`, formData, {
                withCredentials: true,
            })
            .then((response) => {
                if (response.status == 201) {
                    // cookies.set('bearer', response.data.accessToken)
                    setTimeout(() => {
                        navigate('/dashboard');
                    }, 1000);
                }

            })
            .catch((error) => {
                setErrorMessage(error.response.data);
            })
    };

    return (
        <div className="registration-form">
            <h2 className="registration">Registration</h2>
            {/*<div className="registration-form">*/}
            {/*    <div className="form-class">*/}
            <>
                <div>
                    <TextField
                        id="outlined-basic"
                        name="username"
                        label="Username"
                        variant="outlined"
                        value={formData.username}
                        onChange={handleChange}
                    />
                </div>

                <div>
                    <TextField
                        id="outlined-basic"
                        name="password"
                        label="Password"
                        variant="outlined"
                        type="password"
                        value={formData.password}
                        onChange={handleChange}
                    />
                </div>
            </>


            <React.Fragment>
                <Button
                    onClick={handleSubmit}
                >
                    {'Register'}
                </Button>
            </React.Fragment>

            <div>Already have an account? <Link to="/login" className="text-uppercase">Sign in</Link></div>

            <div className="login-error">
                {(errorMessage !== "") ?
                    <Alert severity="error">{errorMessage}</Alert>
                    : <Alert severity="info">Provide your credentials.</Alert>
                }

            </div>

        </div>
    );
};

export default Registration;
