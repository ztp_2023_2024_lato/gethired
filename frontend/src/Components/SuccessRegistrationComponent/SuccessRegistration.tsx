import React from 'react';
import {Link} from "react-router-dom";
import "./SuccessRegistration.css";

const SuccessRegistration = () => {
    return (
        <div className="main-container">
            <h1>You successfully register in our system!</h1>
            <br/>
            <Link to={"/login"}>Go to login page</Link>
        </div>
    );
};

export default SuccessRegistration;