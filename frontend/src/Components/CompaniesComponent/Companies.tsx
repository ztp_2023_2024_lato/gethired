import React, {ChangeEventHandler, useEffect, useState} from 'react';
import 'dayjs/locale/en-gb';
import "./Companies.css";
import debounce from "lodash/debounce";
import dayjs from "dayjs";
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import axios from "axios";
import Cookies from "universal-cookie";
import {
    Modal,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel
} from '@mui/material';
import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import {jwtDecode} from "jwt-decode";
import {useNavigate} from "react-router-dom";
import {visuallyHidden} from '@mui/utils';

dayjs.extend(utc);
dayjs.extend(timezone);

interface Data {
    id: string;
    name: string;
    description: string;
    companySize: number | string;
}

function createData(
    id: string,
    name: string,
    description: string,
    companySize: number | string,
): Data {
    return {
        id,
        name,
        description,
        companySize: typeof companySize === 'string' ? parseInt(companySize, 10) : companySize,
    };
}

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string },
    b: { [key in Key]: number | string },
) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
    disablePadding: boolean;
    id: keyof Data;
    label: string;
    numeric: boolean;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        disablePadding: true,
        label: 'ID',
    },
    {
        id: 'name',
        numeric: false,
        disablePadding: true,
        label: 'Company name',
    },
    {
        id: 'description',
        numeric: false,
        disablePadding: false,
        label: 'Company Description',
    },
    {
        id: 'companySize',
        numeric: true,
        disablePadding: false,
        label: 'Company Size',
    }
];

interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
        props;
    const createSortHandler =
        (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
            onRequestSort(event, property);
        };

    return (
        <TableHead>
            <TableRow>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

interface EnhancedTableToolbarProps {
    numSelected: number;
}

function EnhancedTableToolbar(props: EnhancedTableToolbarProps) {
    const { numSelected } = props;

    return (
        <></>
    );
}


const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'gray',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    color: 'white'
};

const Companies: React.FC = () => {
    const cookies = new Cookies();
    const navigate = useNavigate();
    const [companiesList, setCompaniesList] = useState([]);
    const [open, setOpen] = useState(false);

    const [errorText, setErrorText] = useState('');
    const [username, setUsername] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [companySize, setCompanySize] = useState(0);
    const [companyDescription, setCompanyDescription] = useState('');

    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof Data>('name');
    const [selected, setSelected] = React.useState<readonly number[]>([]);
    const [page, setPage] = React.useState(0);
    const [dense, setDense] = React.useState(false);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: keyof Data,
    ) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = companiesList.map((n: Data) => n.id);
            // @ts-ignore
            setSelected(newSelected);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: string | number) => {
        const selectedIndex = selected.indexOf(Number(id));
        let newSelected: readonly number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, Number(id));
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
        setDense(event.target.checked);
    };

    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    const emptyRows =
        page > 0 ? Math.max(0, (1 + page) * rowsPerPage - companiesList.length) : 0;

    const visibleRows = React.useMemo(
        () =>
            stableSort(companiesList, getComparator(order, orderBy)).slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage,
            ),
        [companiesList, order, orderBy, page, rowsPerPage],
    );

    const handleOpen = () => setOpen(true);
    const handleCreate = () => {
        console.log(companyName, companySize, companyDescription);
        if (companyName === '' || isNaN(companySize) || companyDescription === '') {
            setErrorText('Fill all fields correctly!')
            return
        }

        const token = cookies.get('bearer');
        const decodedToken = jwtDecode(token);

        axios.post(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/company`, {
            name: companyName,
            description: companyDescription,
            companySize: companySize,
            userLogin: decodedToken.sub
        }, {
            headers: {Authorization: `Bearer ${cookies.get('bearer')}`}
        })
            .then((response) => {
                console.log(response)
                setOpen(false)
            })
    };

    const handleShowCompanies = () => {
        navigate('/my-companies');
    }

    const onChange: ChangeEventHandler<HTMLInputElement> = (e) => {
        axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/company`, {
            headers: {Authorization: `Bearer ${cookies.get('bearer')}`},
            params: {
                name: e.target.value
            }
        })
            .then((response) => {
                setCompaniesList(response.data)
            })
    }

    const handleChangeCompanyName: ChangeEventHandler<HTMLInputElement> = (e) => {
        setCompanyName(e.target.value)
    }

    const handleChangeCompanySize: ChangeEventHandler<HTMLInputElement> = (e) => {
        setCompanySize(parseInt(e.target.value))
    }

    const handleChangeCompanyDescription: ChangeEventHandler<HTMLInputElement> = (e) => {
        setCompanyDescription(e.target.value)
    }

    const debouncedOnChange = debounce(onChange, 1000);

    useEffect(() => {
        const fetchCompanies = async () => {
            try {
                const response = await axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/company`, {
                    headers: {Authorization: `Bearer ${cookies.get('bearer')}`},
                });
                setCompaniesList(response.data);
            } catch (error) {
                console.error('Error fetching companies:', error);
            }
        };

        fetchCompanies();
    }, []);


    return (
        <>
            <div className="companies-page">
                <Button onClick={handleOpen}>Create company</Button>
                <Button onClick={handleShowCompanies}>My companies</Button>

                <Modal
                    open={open}
                    onClose={handleCreate}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Create new company
                        </Typography>
                        <div className="form">
                            <TextField id="standard-basic" label="Company name" variant="standard" inputProps={{
                                style: {color: 'white', margin: '0 auto'},
                            }} onChange={handleChangeCompanyName}/>
                            <TextField id="standard-basic" label="Company size" variant="standard" inputProps={{
                                style: {color: 'white', margin: '0 auto'},
                            }} onChange={handleChangeCompanySize}/>
                            <TextField id="standard-basic" label="Description" variant="standard" inputProps={{
                                style: {color: 'white', margin: '0 auto'},
                            }} onChange={handleChangeCompanyDescription}/>
                            <div className="error-text">{errorText}</div>
                            <Button onClick={handleCreate}>Create</Button>
                        </div>
                    </Box>
                </Modal>

                <TextField id="standard-basic" label="Standard" variant="standard" onChange={debouncedOnChange}/>

                <Box sx={{ width: '95%'}}>
                    <Paper sx={{ width: '100%', mb: 2 }}>
                        <EnhancedTableToolbar numSelected={selected.length} />
                        <TableContainer>
                            <Table
                                sx={{ minWidth: 750 }}
                                aria-labelledby="tableTitle"
                                size={dense ? 'small' : 'medium'}
                            >
                                <EnhancedTableHead
                                    numSelected={selected.length}
                                    order={order}
                                    orderBy={orderBy}
                                    onSelectAllClick={handleSelectAllClick}
                                    onRequestSort={handleRequestSort}
                                    rowCount={companiesList.length}
                                />
                                <TableBody>
                                    {visibleRows.map((row, index) => {
                                        // @ts-ignore
                                        const isItemSelected = isSelected(row.id);
                                        const labelId = `enhanced-table-checkbox-${index}`;

                                        return (
                                            <TableRow
                                                hover
                                                onClick={(event) => handleClick(event, row.id)}
                                                role="checkbox"
                                                aria-checked={isItemSelected}
                                                tabIndex={-1}
                                                key={row.id}
                                                selected={isItemSelected}
                                                sx={{ cursor: 'pointer' }}
                                            >
                                                <TableCell
                                                    component="th"
                                                    id={labelId}
                                                    scope="row"
                                                    padding="none"
                                                >
                                                    {row.id}
                                                </TableCell>
                                                <TableCell
                                                    component="th"
                                                    id={labelId}
                                                    scope="row"
                                                    padding="none"
                                                >
                                                    {row.name}
                                                </TableCell>
                                                <TableCell align="right">{row.description}</TableCell>
                                                <TableCell align="right">{row.companySize}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                    {emptyRows > 0 && (
                                        <TableRow
                                            style={{
                                                height: (dense ? 33 : 53) * emptyRows,
                                            }}
                                        >
                                            <TableCell colSpan={6} />
                                        </TableRow>
                                    )}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 25]}
                            component="div"
                            count={companiesList.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onPageChange={handleChangePage}
                            onRowsPerPageChange={handleChangeRowsPerPage}
                        />
                    </Paper>
                </Box>
            </div>

        </>
    );
};

export default Companies;
