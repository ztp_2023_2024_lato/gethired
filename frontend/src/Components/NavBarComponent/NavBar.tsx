// AppNavbar.tsx

import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import {Link, useLocation} from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Button from '@mui/material/Button';
import AdbIcon from '@mui/icons-material/Adb';
import Tooltip from '@mui/material/Tooltip';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { JwtPayload } from 'jwt-decode';
import Cookies from 'universal-cookie';
import './NavBar.css';
import {useAuth} from "../../support-functions/auth-provider";
import imageToAdd from "../../Assets/logo.png";
interface IButton {
    title: string;
    url: string;
}

const pages: IButton[] = [
    { title: 'Your Stocks', url: '/exchange-for' },
    { title: 'Live Chart', url: '/live-chart' },
];

const settings: IButton[] = [
    { title: 'Profile', url: '/profile' },
    { title: 'Dashboard', url: '/dashboard' },
    // { title: 'Settings', url: '/settings' },
    { title: 'Logout', url: '/logout' },
];

const LoginButtons: IButton[] = [
    { title: 'Login', url: '/login' },
    { title: 'Register', url: '/signup' },
];

const PagesButtons: IButton[] = [
    { title: 'Vacancies', url: '/vacancies' },
    { title: 'Companies', url: '/companies' },
];

const AppNavbar: React.FC<{
    user: JwtPayload;
    cookies: Cookies;
    loggedIn: boolean;
    setLoggedIn: Dispatch<SetStateAction<boolean>>;
}> = ({ user, cookies, loggedIn, setLoggedIn }) => {
    // const [settings, setSettings] = useState<>([]);

    const { getUser, userIsAuthenticated, userLogout } = useAuth();
    const Auth = useAuth();
    const currentPathName = useLocation().pathname;

    useEffect(() => {

        if (cookies.get('bearer')) {
            setLoggedIn(true);
        } else {
            setLoggedIn(false);
        }

    }, [currentPathName]);


    const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
    const [anchorElLogin, setAnchorElLogin] = useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleOpenLoginMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElLogin(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleCloseLoginMenu = () => {
        setAnchorElLogin(null);
    };

    return (
        <AppBar style={{background: '#000',boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.1)',position: 'sticky' }} position="static" className="AppBar">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    {/*<Button component={Link} to={"/"} >*/}
                    {/*   <img src={imageToAdd} alt="Image" style={{width:'80px'}}  />*/}
                    {/*</Button>*/}

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon className="menuIcon" />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {PagesButtons.map((page, key) => (
                                <Button
                                    key={key}
                                    onClick={handleCloseNavMenu}
                                    sx={{
                                        display: 'block',
                                        my: '2',
                                        textTransform: 'none',
                                        textAlign: 'center',
                                    }}
                                    component={Link}
                                    to={page.url}
                                    className="navButtons"
                                >
                                    {page.title}
                                </Button>
                            ))}
                        </Menu>
                    </Box>

                    <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {loggedIn ? (PagesButtons.map((page, key) => (
                            <Button
                                key={key}
                                onClick={handleCloseNavMenu}
                                sx={{ my: 2, color: 'white', display: 'block' }}
                                component={Link}
                                to={page.url}
                                className="navButtons"
                            >
                                {page.title}
                            </Button>
                        ))) : (<div></div>)}
                    </Box>

                    {loggedIn ? (
                        <Box sx={{ flexGrow: 0 }} className="userAvatar">
                            <Tooltip title="Open settings">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting, key) => (
                                    <Button
                                        key={key}
                                        onClick={handleCloseUserMenu}
                                        sx={{
                                            display: 'block',
                                            my: '2',
                                            textTransform: 'none',
                                            textAlign: 'center',
                                        }}
                                        component={Link}
                                        to={setting.url}
                                        className="navButtons"
                                    >
                                        {setting.title}
                                    </Button>
                                ))}
                            </Menu>
                        </Box>
                    ) : (
                        <div>
                            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                                {LoginButtons.map((button, key) => (
                                    <Button
                                        key={key}
                                        onClick={handleCloseLoginMenu}
                                        sx={{
                                            my: 2,
                                            color: 'white',
                                            display: 'block',
                                        }}
                                        component={Link}
                                        to={button.url}
                                        className="loginButtons"
                                    >
                                        {button.title}
                                    </Button>
                                ))}
                            </Box>

                            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleOpenLoginMenu}
                                    color="inherit"
                                >
                                    <MenuIcon className="menuIcon" />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorElLogin}
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'left',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'left',
                                    }}
                                    open={Boolean(anchorElLogin)}
                                    onClose={handleCloseLoginMenu}
                                    sx={{
                                        display: { xs: 'block', md: 'none' },
                                    }}
                                >
                                    {LoginButtons.map((button, key) => (
                                        <Button
                                            key={key}
                                            onClick={handleCloseLoginMenu}
                                            sx={{
                                                display: 'block',
                                                my: '2',
                                                textTransform: 'none',
                                                textAlign: 'center',
                                            }}
                                            component={Link}
                                            to={button.url}
                                            className="loginButtons"
                                        >
                                            {button.title}
                                        </Button>
                                    ))}
                                </Menu>
                            </Box>
                        </div>
                    )}
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default AppNavbar;
