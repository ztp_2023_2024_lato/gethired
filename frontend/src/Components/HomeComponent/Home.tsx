import React from 'react';
import '../../App.css';
import AppNavbar from '../NavBarComponent/NavBar';
import {Link} from 'react-router-dom';
import {Button, Container} from 'reactstrap';

const Home = () => {
    return (
        <div className={"main-container"}>

            <Container fluid>

                    GetHired
                    <br/>
                    Find your dream job today!

            </Container>
        </div>
    );
}

export default Home;