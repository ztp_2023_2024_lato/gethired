import React, {ChangeEventHandler, useEffect, useState} from 'react';
import "./MyCompanies.css";
import {Button, Container} from "reactstrap";
import {Link, useNavigate} from "react-router-dom";
import axios from "axios";
import Cookies from "universal-cookie";
import {jwtDecode} from "jwt-decode";
import {Modal, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'gray',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    color: 'white'
};

const MyCompanies: React.FC = () => {
    const navigate = useNavigate();

    const [open, setOpen] = useState(false);
    const cookies = new Cookies();
    const [userCompanies, setUserCompanies] = useState([]);
    const [selectedId, setSelectedId] = useState('');

    const [vacancyTitle, setVacancyTitle] = useState('');
    const [vacancyDescription, setVacancyDescription] = useState('');

    const handleOpen = (e: any) => {
        setSelectedId(e.target.id)
        setVacancyTitle('');
        setVacancyDescription('');
        setOpen(true)
    };

    const handlePost = () => {
        console.log(123);
        axios.post(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/vacancy`, {
            companyId: selectedId,
            title: vacancyTitle,
            description: vacancyDescription
        }, {
            headers: {Authorization: `Bearer ${cookies.get('bearer')}`}
        })
            .then((response) => {
                console.log(response)
            })
    }

    const handleClose = () => setOpen(false);

    const handleChangeVacancyTitle = (e: any) => {
        setVacancyTitle(e.target.value)
    }

    const handleChangeVacancyDescription = (e: any) => {
        setVacancyDescription(e.target.value)
    }

    const handleCheckApplications = (e: any) => {
        navigate('/applications', {state: {companyId: e.target.id}});
    }

    useEffect(() => {
        const token = cookies.get('bearer');
        const decodedToken = jwtDecode(token);
        axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/company/access`, {
                headers: {Authorization: `Bearer ${cookies.get('bearer')}`},
                params: {
                    userLogin: decodedToken.sub
                }
            },
        )
            .then((response) => {
                setUserCompanies(response.data);
            })
    }, []);

    return (
        <div>
            <div className="my-companies-page">
                <Modal
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Create new vacancy
                        </Typography>
                        <div className="form">
                            <TextField id="standard-basic" label="Vacancy title" variant="standard" inputProps={{
                                style: {color: 'white', margin: '0 auto'},
                            }} onChange={handleChangeVacancyTitle}/>
                            <TextField id="standard-basic" label="Description" variant="standard" inputProps={{
                                style: {color: 'white', margin: '0 auto'},
                            }} onChange={handleChangeVacancyDescription}/>
                            <Button onClick={handlePost}>Create</Button>
                        </div>
                    </Box>
                </Modal>

                <TableContainer component={Paper} style={{height: 100 + '%'}}>
                    <Table sx={{minWidth: 650}} aria-label="simple table">
                        <TableHead sx={{bgcolor: '#000000'}}>
                            <TableRow>
                                <TableCell sx={{color: '#ffffff'}}>Company name</TableCell>
                                <TableCell align="right" sx={{color: '#ffffff'}}>Company Description</TableCell>
                                <TableCell align="right" sx={{color: '#ffffff'}}>Company Size</TableCell>
                                <TableCell align="right" sx={{color: '#ffffff'}}>Create</TableCell>
                                <TableCell align="right" sx={{color: '#ffffff'}}>Check applications</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {userCompanies.map((company: any) => (
                                <TableRow
                                    key={company.name}
                                    sx={{
                                        '&:last-child td, &:last-child th': {border: 0},
                                        '&:nth-child(odd)': {bgcolor: '#23262F'},
                                        '&:nth-child(even)': {bgcolor: '#5b5e6c'}
                                    }}
                                >
                                    <TableCell component="th" scope="row" sx={{color: '#ffffff'}}>
                                        {company.name}
                                    </TableCell>
                                    <TableCell align="right" sx={{color: '#ffffff'}}>{company.description}</TableCell>
                                    <TableCell align="right" sx={{color: '#ffffff'}}>{company.companySize}</TableCell>
                                    <TableCell align="right" sx={{color: '#ffffff'}}>
                                        <Button onClick={handleOpen} id={company.id}>Create new job</Button>
                                    </TableCell>
                                    <TableCell align="right" sx={{color: '#ffffff'}}>
                                        <Button onClick={handleCheckApplications} id={company.id}>Check applications</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    );
};

export default MyCompanies;