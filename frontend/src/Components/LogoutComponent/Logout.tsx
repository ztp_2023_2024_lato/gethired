import React, {Dispatch, SetStateAction, useEffect} from 'react';
import {redirect, useNavigate} from "react-router-dom";
import Cookies from "universal-cookie";
import RedirectLoggedInProvider from "../../support-functions/redirect-logged-in-provider";
import {useAuth} from "../../support-functions/auth-provider";

const Logout: React.FC<{cookies: Cookies}> = ({cookies}) => {
    const navigate = useNavigate();

    useEffect(() => {
        setTimeout(() => {
            cookies.remove('bearer');
            navigate('/');
        }, 1000);
    }, []);

    return (
        <div className={"main-container"}>
            {/*<RedirectLoggedInProvider />*/}
            You successfully logout! Redirecting.
        </div>
    );
};

export default Logout;