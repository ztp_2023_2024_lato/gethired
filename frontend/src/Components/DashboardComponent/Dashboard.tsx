import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, ThemeProvider} from "@mui/material";
import Cookies from "universal-cookie";
import {jwtDecode} from "jwt-decode";
import './Dashboard.css';

const Dashboard: React.FC = () => {
    const cookies = new Cookies();
    const [userApplications, setUserApplications] = useState([]);

    useEffect(() => {
        const token = cookies.get('bearer');
        const decodedToken = jwtDecode(token);

        axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/users/${decodedToken.sub}/applications`, {
            headers: {Authorization: `Bearer ${token}`}
        }).then((response) => {
            setUserApplications(response.data);
            // console.log(response.data);
        })
    }, []);

    return (
        <div className={"main-container"}>

            My applications:

            <TableContainer component={Paper} style={{height: 100 + '%'}}>
                <Table sx={{minWidth: 650}} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Position</TableCell>
                            <TableCell align="right">Description</TableCell>
                            <TableCell align="right">Company Name</TableCell>
                            <TableCell align="right">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userApplications.map((application: any) => (
                            <TableRow
                                key={application.id}
                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                            >
                                <TableCell component="th" scope="row">
                                    {application.vacancy.title}
                                </TableCell>
                                <TableCell align="right">{application.vacancy.description}</TableCell>
                                <TableCell align="right">{application.vacancy.company.name}</TableCell>
                                <TableCell align="right">{application.status}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

        </div>
    );
}
export default Dashboard;