import React, {useEffect, useState} from 'react';
import 'dayjs/locale/en-gb';
import "./Applications.css";
import dayjs from "dayjs";
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import {Modal, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import axios from "axios";
import Cookies from "universal-cookie";
import {Button} from "reactstrap";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import {jwtDecode} from "jwt-decode";
import {useLocation} from "react-router-dom";

dayjs.extend(utc);
dayjs.extend(timezone);

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'gray',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    color: 'white'
};

const Applications: React.FC = () => {

    const [applicationList, setApplicationList] = useState([]);
    const cookies = new Cookies();
    const { state } = useLocation();

    useEffect(() => {
        const token = cookies.get('bearer');
        const decodedToken = jwtDecode(token);

        console.log(state.companyId);
        axios.get(`http://${process.env.REACT_APP_SERVER_DNS_URL}/api/application/${state.companyId}`, {
            headers: {Authorization: `Bearer ${token}`}
        })
            .then((response) => {
                console.log(response.data);
                setApplicationList(response.data);
            })
    }, []);

    return (
        <>
            <div className="applications-page">
                <TableContainer component={Paper} style={{height: 100 + '%'}}>
                    <Table sx={{minWidth: 650}} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>User</TableCell>
                                <TableCell align="right">Vacancy</TableCell>
                                <TableCell align="right">Company Name</TableCell>
                                <TableCell align="right">Cover Letter</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                                {applicationList.map((application: any) => (
                                    <TableRow
                                        key={application.id}
                                        sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                    >
                                        <TableCell component="th" scope="row">
                                            {application.user.username}
                                        </TableCell>
                                        <TableCell align="right">{application.vacancy.title}</TableCell>
                                        <TableCell align="right">{application.vacancy.company.name}</TableCell>
                                        <TableCell align="right">
                                            <b>{application.text}</b>
                                        </TableCell>

                                    </TableRow>
                                ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>

        </>
    );
};

export default Applications;
