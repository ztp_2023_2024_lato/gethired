import React, {useState} from 'react';
import axios from 'axios';
import Button from "@mui/material/Button";
import {Alert, TextField} from "@mui/material";
import "./Login.css";
import {useNavigate} from 'react-router-dom';
import Cookies from "universal-cookie";
import {JwtPayload} from "jwt-decode";
import {useAuth} from "../../support-functions/auth-provider";

interface LoginDto {
    username: string;
    password: string;
}

const Login: React.FC<{ user: JwtPayload, setUser: any }> = ({user, setUser}) => {
    const [formData, setFormData] = useState<LoginDto>({username: '', password: ''});
    const [correctCredentials, setCorrectCredentials] = useState<boolean>();
    const navigate = useNavigate();
    const cookies = new Cookies();
    const Auth = useAuth();

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        console.log('Sending login data:', formData);
        axios.post(
            `http://${process.env.REACT_APP_SERVER_DNS_URL}/api/auth/login`,
            formData,
            { withCredentials: true }
        ).then((response) => {
            setCorrectCredentials(true);
            cookies.set('bearer', response.data.accessToken)
            setTimeout(() => {
                navigate('/dashboard');
            }, 3000);
        }).catch((error) => {
            console.log(error);
            setCorrectCredentials(false);
        });
    };

    return (
        <div className="login-page">
            <h2 className="login">Login</h2>
            <h6 className="description">Please, input your credentials.</h6>
            <form onSubmit={handleSubmit} className="form-class">
                <div>
                    <TextField id="outlined-basic" name="username" label="Username" variant="outlined"
                               value={formData.username} onChange={handleChange}/>
                </div>
                <div>
                    <TextField id="outlined-basic" name="password" label="Password" variant="outlined" type="password"
                               value={formData.password} onChange={handleChange}/>
                </div>
                <Button variant="contained" type="submit">Login</Button>
            </form>
            <div className="login-error">
                {(correctCredentials === false) ?
                    <Alert severity="error">Please, provide correct credentials!</Alert>
                    : (correctCredentials === true) ?
                        <Alert severity="success">You successfully logged in. Redirecting....</Alert>
                        : <Alert severity="info">Provide your credentials.</Alert>
                }

            </div>
        </div>
    );
};

export default Login;