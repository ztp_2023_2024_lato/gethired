import React, {useState} from 'react';
import './App.css';
import Home from './Components/HomeComponent/Home';
import Registration from './Components/RegistrationComponent/Registration';
import Login from './Components/LoginComponent/Login';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './Components/NavBarComponent/NavBar';
import {ThemeProvider} from "@mui/material";
import theme from "./miu/theme";
import Cookies from "universal-cookie";
import {JwtPayload} from "jwt-decode";
import {AuthProvider} from "./support-functions/auth-provider";
import "./support-functions/i18n/i18n";
import Dashboard from "./Components/DashboardComponent/Dashboard";
import Companies from "./Components/CompaniesComponent/Companies";
import Vacancies from "./Components/VacanciesComponent/Vacancies";
import RedirectLoggedInProvider from "./support-functions/redirect-logged-in-provider";
import RedirectLoggedOutProvider from "./support-functions/redirect-logged-out-provider";
import Logout from "./Components/LogoutComponent/Logout";
import Profile from "./Components/ProfileComponent/Profile";
import MyCompanies from "./Components/MyCompaniesComponent/MyCompanies";
import Applications from "./Components/ApplicationsComponent/Applications";


const App = () => {
    const cookies = new Cookies();
    const [user, setUser] = useState<JwtPayload>({});
    const [loggedIn, setLoggedIn] = useState(false);
    return (
        <AuthProvider>
            <Router>
                <ThemeProvider theme={theme}>
                    <AppNavbar user={user} cookies={cookies} loggedIn={loggedIn} setLoggedIn={setLoggedIn}/>
                    <Routes>
                        <Route element={<RedirectLoggedOutProvider/>}>
                            <Route path="/" element={<Home/>}/>
                            <Route path='/signup' element={<Registration user={user} setUser={setUser}/>}/>
                            <Route path='/login' element={<Login user={user} setUser={setUser}/>}/>
                        </Route>
                        <Route element={<RedirectLoggedInProvider/>}>
                            <Route path="/dashboard" element={<Dashboard/>}/>
                            <Route path="/companies" element={<Companies/>}/>
                            <Route path="/vacancies" element={<Vacancies/>}/>
                            <Route path={"/profile"} element={<Profile/>}/>
                            <Route path={"/my-companies"} element={<MyCompanies/>}/>
                            <Route path={"/applications"} element={<Applications/>}/>
                            <Route path="/logout" element={<Logout cookies={cookies}/>}/>
                        </Route>
                    </Routes>
                </ThemeProvider>
            </Router>
        </AuthProvider>

    )
}

export default App;