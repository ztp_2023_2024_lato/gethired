import jwt, { decode } from 'jsonwebtoken';
export default function verifyToken(token: string): boolean {
    try {
        const decodedToken = jwt.verify(token, 'eyJhbGciOiJIUzI1NiJ9eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY5OTIxMDAwNSwiaWF0IjoxNjk5MjEwMDA1fQHg1l3otzVVXQP47HD2Nj7yGlwX5wLLt0K0shEwFaS8'); // Replace 'your-secret-key' with your actual secret key
        const currentTime = (Date.now() / 1000).toString();
        const  exp  = decode(token);
        // Check if the token is not expired
        if (decodedToken && exp && exp > currentTime) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        console.error('Token verification error:', error);
        return false;
    }
}