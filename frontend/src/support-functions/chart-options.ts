export const options = { scales: { x: { display: false } } }

export const predictionsOptions = {
    scales: {
        x: {
            display: false
        },
        y: {
            ticks: {
                stepSize: 40000
            }
        }
    }
}