const regex = {
    'firstName': `\\b([A-ZÀ-ÿ][-,a-z. ']+[ ]*)+`,
    'lastName': `\\b([A-ZÀ-ÿ][-,a-z. ']+[ ]*)+`,
}

export default regex;