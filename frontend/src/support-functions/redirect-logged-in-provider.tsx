import React from 'react';
import {useAuth} from "./auth-provider";
import {Navigate, Outlet} from "react-router-dom";
import Cookies from "universal-cookie";

const RedirectLoggedInProvider = () => {
    const Auth = useAuth()
    const cookies = new Cookies();
    const cookiesList = cookies.getAll();

    return (
        <div>
            {cookiesList.bearer === undefined ? (<Navigate to={'/login'}/>) : <Outlet />}
        </div>
    );
};

export default RedirectLoggedInProvider;