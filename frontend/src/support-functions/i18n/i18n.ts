import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translationEn from "../../translates/en.json";
import translationPl from "../../translates/pl.json";

i18n
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translation: translationEn
            },
            pl: {
                translation: translationPl
            }
        },
        fallbackLng: "en",
        debug: true,

        // have a common namespace used around the full app
        ns: ["translations"],
        defaultNS: "translations",

        keySeparator: false, // we use content as keys

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;