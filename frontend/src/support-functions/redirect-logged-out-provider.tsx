import React from 'react';
import {useAuth} from "./auth-provider";
import {Navigate, Outlet} from "react-router-dom";
import Cookies from "universal-cookie";

const RedirectLoggedOutProvider = () => {
    const Auth = useAuth()
    const cookies = new Cookies();
    const cookiesList = cookies.getAll();

    return (
        <div className="outlet-container">
            {cookiesList.bearer === undefined ? <Outlet /> : (<Navigate to={'/dashboard'}/>)}
        </div>
    );
};

export default RedirectLoggedOutProvider;