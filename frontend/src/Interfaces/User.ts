export interface IUser {
    id: number,
    username: string,
    roles: IRole[],
    email: string,
    authData: string,
}

interface IRole {
    Role: string
}