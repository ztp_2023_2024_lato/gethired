export interface IGroups {
    groups: IGroup[]
}

interface IGroup {
    id: number,
    name: string,
    address: string | null,
    city: string | null,
    stateOrProvince: string | null,
    country: string | null,
    postalCode: string | null,
    user: string | null,
    events: IEvent[] | []
}

interface IEvent {
    id: number,
    date: string,
    title: string,
    description: string,
    attendees: [],
}