FROM gradle:8.6.0-jdk17-alpine AS build

COPY src/main /gethired/src/main
COPY *.gradle.kts /gethired/

WORKDIR /gethired

RUN gradle clean build

FROM openjdk:17-alpine AS run

COPY --from=build /gethired/build/libs/*.jar /app/libs/app.jar
COPY --from=build /gethired/build/resources /app/resources

WORKDIR /app

EXPOSE 8080

CMD ["java", "-Dspring.profiles.active=${SPRING_PROFILE}", "-jar", "libs/app.jar"]