# GetHired project

![img_8.png](screenshots/img_8.png)
```
Offers flickering bright,
Virtual path towards the goal,
Work within reach.
           
                     - ChatGPT
```

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [REST API methods](#rest-api-methods)
* [Database Diagram](#database-diagram)
* [Features](#features)
* [Contact](#contact)

## General Information
Project was founded by a group of students from Cracow University of Technology. 
The main goal of the project is to make job board where companies can post job offers and candidates can apply to them.

Our application is a simple and effective tool that facilitates job searching and recruitment. With flexible posting options for employers and user-friendly navigation, our platform enables swift connections between employers and candidates. It's the perfect solution for individuals seeking employment across various industries and experience levels, as well as for companies aiming to find the best-fitting candidates for their needs.

## Technologies Used
- React
- TypeScript
- Spring Boot
- Kotlin
- PostgreSQL
- Docker
- Docker Compose
- JWT
- Hibernate
- Swagger
- Amazon Web Services

## REST API methods
- UserController
![img.png](screenshots/img.png)
- CompanyController
![img_1.png](screenshots/img_1.png)
- VacancyController
![img_2.png](screenshots/img_2.png)
- ApplicationController
![img_3.png](screenshots/img_3.png)
- AuthController
![img_4.png](screenshots/img_4.png)
- AuthenticationController
![img_5.png](screenshots/img_5.png)
- CompanyAccessController
![img_6.png](screenshots/img_6.png)

## Database Diagram
![img_7.png](screenshots/img_7.png)

## Features
- Creating account and login to application
- See your dashboard and your applies
- Update your profile
- Create company account
- Create job offer
- Check all job offers
- Apply to job offer
- Check all applies to your job offers

## Contact
Created by:
- [Aliaksei Lomats](https://github.com/lomaTT)
- [Serhii Cherepiuk](https://github.com/SergeyCherepiuk)
