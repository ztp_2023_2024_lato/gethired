package com.gethired.backend.controller

import com.gethired.backend.dto.company.CreateCompanyRequestDto
import com.gethired.backend.dto.company.UpdateCompanyRequestDto
import com.gethired.backend.model.Company
import com.gethired.backend.repository.CompanyRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import java.util.*

class TestCompanyController {
    @InjectMocks
    private lateinit var companyController: CompanyController

    @Mock
    private lateinit var companyRepository: CompanyRepository

    init {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test company creation with invalid name`() {
        val createCompanyRequestDto = CreateCompanyRequestDto(
            "a",
            "testdescription",
            10,
            "testuserlogin",
        )

        val responseEntity = companyController.createCompany(createCompanyRequestDto)

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }

    @Test
    fun `test updating company with invalid name`() {
        val companyIdTest = UUID.randomUUID()
        val companyTest = Company(
            companyIdTest,
            "testcompany1",
            "testdescription",
            10
        )
        Mockito.`when`(companyRepository.findById(companyIdTest)).thenReturn(Optional.of(companyTest))

        val updateCompanyIdTest = UUID.randomUUID()
        val updateCompanyTest = Company(
            updateCompanyIdTest,
            "a",
            "desc",
            10
        )

        val responseEntity = companyTest.let { company ->
            companyController.updateCompany(
                company.id,
                UpdateCompanyRequestDto(
                    updateCompanyTest.name,
                    updateCompanyTest.description,
                    updateCompanyTest.companySize
                )
            )
        }

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.statusCode)
    }
}