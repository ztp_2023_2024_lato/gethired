package com.gethired.backend.controller

import com.gethired.backend.dto.authentication.RegisterRequestDto
import com.gethired.backend.dto.user.UpdateUserRequestDto
import com.gethired.backend.model.User
import com.gethired.backend.repository.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.*

class TestUserController {
    @InjectMocks
    private lateinit var authenticationController: AuthenticationController

    @InjectMocks
    private lateinit var userController: UserController

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var passwordEncoder: PasswordEncoder

    init {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test user registration`() {
        val registerRequestDto = RegisterRequestDto("testuser", "StrongPassword123")
        val encodedPassword = "encodedPassword"
        `when`(passwordEncoder.encode("StrongPassword123")).thenReturn(encodedPassword)

        val responseEntity = authenticationController.register(registerRequestDto)

        verify(userRepository).save(any(User::class.java))
        Assertions.assertEquals(HttpStatus.CREATED, responseEntity.statusCode)
    }

    @Test
    fun `test finding user by username`() {
        val testUserId = "testuser"
        val testUser = User(testUserId, "encodedPassword") // Assuming you have a constructor for User class
        `when`(userRepository.findById(testUserId)).thenReturn(Optional.of(testUser))

        val responseEntity = userController.getUserById(testUserId)

        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode)
    }

    @Test
    fun `test user updating by username`() {
        val testUserId = "testuser"
        val testUser = User(testUserId, "encodedPassword")
        `when`(userRepository.findById(testUserId)).thenReturn(Optional.of(testUser))

        val responseEntity = userController.updateUser(testUserId, UpdateUserRequestDto("newUsername"))

        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode)
    }

    @Test
    fun `test user deleting`() {
        val testUserId = "testuser"
        val testUser = User(testUserId, "encodedPassword")
        `when`(userRepository.findById(testUserId)).thenReturn(Optional.of(testUser))

        val responseEntity = userController.deleteUser(testUserId)

        Assertions.assertEquals(HttpStatus.OK, responseEntity.statusCode)
    }
}