create table users (
    username varchar primary key,
    password varchar not null
);

create table companies (
    id uuid primary key,
    name varchar not null,
    description varchar not null,
    company_size integer not null,
    logo_url varchar not null
);

create table company_accesses (
    id uuid primary key,
    company_id uuid not null references companies(id),
    username varchar not null unique,
    password varchar not null
);

create table skill_tags (
    name varchar primary key
);

create table vacancies (
    id uuid primary key,
    title varchar not null,
    description varchar not null
    -- salary_range
);

create table vacancies_skill_tags (
    vacancy_id uuid not null references vacancies(id),
    skill_tag_name varchar not null references skill_tags(name),
    primary key(vacancy_id, skill_tag_name)
);

create table applications (
    id uuid primary key,
    user_username varchar not null references users(username),
    vacancy_id uuid not null references vacancies(id),
    status varchar not null
);