package com.gethired.backend.controller

import com.gethired.backend.dto.company.CreateCompanyRequestDto
import com.gethired.backend.dto.company.GetCompanyResponseDto
import com.gethired.backend.dto.company.UpdateCompanyRequestDto
import com.gethired.backend.model.Company
import com.gethired.backend.model.CompanyAccess
import com.gethired.backend.repository.CompanyAccessRepository
import com.gethired.backend.repository.CompanyRepository
import com.gethired.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/api/company")
class CompanyController {

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var companyAccessRepository: CompanyAccessRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @GetMapping("/{id}")
    fun getCompanyById(@PathVariable id: UUID): ResponseEntity<GetCompanyResponseDto> {
        val result = companyRepository.findById(id)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        val company = result.get()
        val response = GetCompanyResponseDto(
            id = company.id.toString(),
            name = company.name,
            description = company.description,
            companySize = company.companySize,
            logoUrl = company.logoUrl,
        )
        return ResponseEntity.ok(response)
    }

    @PostMapping("")
    fun createCompany(@RequestBody company: CreateCompanyRequestDto): ResponseEntity<String> {
        val badRequest = !Validator.validCompanyName(company.name)
                || !Validator.validCompanyDescription(company.description)
                || !Validator.validCompanySize(company.companySize)

        if (badRequest) return ResponseEntity.badRequest().build()

        val companyInfo = Company(
            name = company.name,
            description = company.description,
            companySize = company.companySize,
        )

        val headUser = userRepository.findById(company.userLogin)
        if (!headUser.isPresent) return ResponseEntity.badRequest().build()

        val companyAccess = CompanyAccess(
            headUser = headUser.get(),
            company = companyInfo,
        )

        companyRepository.save(companyInfo)
        companyAccessRepository.save(companyAccess);
        return ResponseEntity("Company created successfully", HttpStatus.CREATED)
    }

    @PutMapping("/{id}")
    fun updateCompany(
        @PathVariable id: UUID,
        @RequestBody updates: UpdateCompanyRequestDto,
    ): ResponseEntity<String> {
        val badRequest = (updates.name != null && !Validator.validCompanyName(updates.name))
                || (updates.description != null && !Validator.validCompanyDescription(updates.description))
                || (updates.companySize != null && !Validator.validCompanySize(updates.companySize))
        if (badRequest) return ResponseEntity.badRequest().build()

        val result = companyRepository.findById(id)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        var company = result.get()
        company = company.copy(
            name = updates.name ?: company.name,
            description = updates.description ?: company.description,
            companySize = updates.companySize ?: company.companySize,
            logoUrl = updates.logoUrl ?: company.logoUrl,
        )

        companyRepository.save(company)
        return ResponseEntity.ok("Company updated successfully")
    }

    @DeleteMapping("/{id}")
    fun deleteCompany(@PathVariable id: UUID) = companyRepository.deleteById(id)

    @GetMapping("")
    fun getAllCompanies(@RequestParam("name") name: String = ""): ResponseEntity<List<GetCompanyResponseDto>> {
        val result: List<Company> = if (name.isNotEmpty()) {
            companyRepository.findByName(name)
        } else {
            companyRepository.findAll()
        }

        val companies = result.map { company ->
            GetCompanyResponseDto(
                id = company.id.toString(),
                name = company.name,
                description = company.description,
                companySize = company.companySize,
                logoUrl = company.logoUrl,
            )
        }

        return ResponseEntity.ok(companies)
    }
}