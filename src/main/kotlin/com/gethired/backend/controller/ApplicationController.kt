package com.gethired.backend.controller

import com.gethired.backend.dto.application.CreateApplicationRequestDto
import com.gethired.backend.model.Application
import com.gethired.backend.model.ApplicationStatus
import com.gethired.backend.repository.ApplicationRepository
import com.gethired.backend.repository.UserRepository
import com.gethired.backend.repository.VacancyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/api/application")
class ApplicationController {

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    @Autowired
    lateinit var vacancyRepository: VacancyRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @PostMapping("")
    fun createApplication(@RequestBody application: CreateApplicationRequestDto): ResponseEntity<String> {

        val vacancy = vacancyRepository.findById(UUID.fromString(application.vacancyId));
        if (!vacancy.isPresent) return ResponseEntity.notFound().build()

        val user = userRepository.findById(application.userId);
        if (!user.isPresent) return ResponseEntity.notFound().build()

        val applicationInfo = Application(
            user = user.get(),
            vacancy = vacancy.get(),
            status = ApplicationStatus.SUBMITTED.name,
            text = application.text,
        )

        applicationRepository.save(applicationInfo);

        return ResponseEntity("Application created successfully", HttpStatus.CREATED)
    }

    @GetMapping("/{companyId}")
    fun findCompanyApplications(@PathVariable companyId: UUID): ResponseEntity<List<Application>> {
        val applications = applicationRepository.findAllByCompany(UUID.fromString(companyId.toString()));

        return ResponseEntity.ok(applications);
    }

}