package com.gethired.backend.controller

object Validator {
    private val REGEX_EMAIL = Regex("^[^@]+@[^@]+\\.[^@]+\$")
    private val REGEX_PASSWORD = Regex("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}\$")

    fun validEmail(email: String) = REGEX_EMAIL.matchEntire(email) != null
    fun validPassword(password: String) = REGEX_PASSWORD.matchEntire(password) != null

    fun validCompanyName(name: String) = name.length > 3
    fun validCompanyDescription(description: String) = description.length < 3000
    fun validCompanySize(companySize: Int) = companySize > 0
}