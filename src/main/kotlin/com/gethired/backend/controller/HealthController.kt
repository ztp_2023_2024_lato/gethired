package com.gethired.backend.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/health")
class HealthController {

    @GetMapping
    fun health() = ResponseEntity(null, HttpStatus.OK)

}