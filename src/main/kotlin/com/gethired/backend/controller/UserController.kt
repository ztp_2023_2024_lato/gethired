package com.gethired.backend.controller

import com.gethired.backend.dto.user.GetUserResponseDto
import com.gethired.backend.dto.user.UpdateUserRequestDto
import com.gethired.backend.model.Application
import com.gethired.backend.repository.ApplicationRepository
import com.gethired.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/api/users")
class UserController {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    @GetMapping("/{username}")
    fun getUserById(@PathVariable username: String): ResponseEntity<GetUserResponseDto> {
        val result = userRepository.findById(username)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        val user = result.get()
        val response = GetUserResponseDto(user.username)
        return ResponseEntity.ok(response)
    }

    @GetMapping("/{username}/applications")
    fun getUserApplications(@PathVariable username: String): ResponseEntity<List<Application>> {
        val result = applicationRepository.findByUserUsername(username);

        val applications = result.toList()
        return ResponseEntity.ok(applications)
    }

    @PutMapping("/{username}")
    fun updateUser(
        @PathVariable username: String,
        @RequestBody updates: UpdateUserRequestDto,
    ): ResponseEntity<String> {
        updates.username?.let {
            if (userRepository.existsById(it)) {
                return ResponseEntity("Username is taken", HttpStatus.BAD_REQUEST)
            }
        }

        val result = userRepository.findById(username)
        if (!result.isPresent) return ResponseEntity.notFound().build()

        var user = result.get()
        user = user.copy(
            username = updates.username ?: user.username,
        )

        userRepository.save(user)

        return ResponseEntity("Updates are saved successfully", HttpStatus.OK)
    }

    @DeleteMapping("/{username}")
    fun deleteUser(@PathVariable username: String): ResponseEntity<String> {
        // TODO: Make not found if user does not exist
        userRepository.deleteById(username)

        return ResponseEntity("User is deleted successfully", HttpStatus.OK)
    }

}