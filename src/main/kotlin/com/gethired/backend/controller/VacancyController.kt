package com.gethired.backend.controller;

import com.gethired.backend.dto.vacancy.CreateVacancyRequestDto
import com.gethired.backend.dto.vacancy.GetVacanciesResponseDTO
import com.gethired.backend.model.Vacancy
import com.gethired.backend.repository.CompanyRepository
import com.gethired.backend.repository.VacancyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/api/vacancy")
public class VacancyController {

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var vacancyRepository: VacancyRepository

    @PostMapping("")
    fun createVacancy(@RequestBody vacancy: CreateVacancyRequestDto): ResponseEntity<String> {

        val company = companyRepository.findById(UUID.fromString(vacancy.companyId));
        if (!company.isPresent) return ResponseEntity.notFound().build()

        val vacancyInfo = Vacancy(
            title = vacancy.title,
            description = vacancy.description,
            company = company.get(),
        )

        vacancyRepository.save(vacancyInfo);

        return ResponseEntity("Vacancy created successfully", HttpStatus.CREATED)
    }

    @GetMapping("")
    fun getVacancies(): ResponseEntity<List<GetVacanciesResponseDTO>> {
        val vacancies = vacancyRepository.findAll();

        val vacanciesList = vacancies.map {
            GetVacanciesResponseDTO(
                id = it.id.toString(),
                title = it.title,
                description = it.description,
                companyName = it.company.name,
            )
        }

        return ResponseEntity.ok(vacanciesList);
    }
}
