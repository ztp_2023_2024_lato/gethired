package com.gethired.backend.controller

import com.gethired.backend.dto.company.GetCompanyResponseDto
import com.gethired.backend.model.Company
import com.gethired.backend.model.CompanyAccess
import com.gethired.backend.repository.CompanyAccessRepository
import com.gethired.backend.repository.CompanyRepository
import com.gethired.backend.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin(originPatterns = ["*"])
@RestController
@RequestMapping("/api/company/access")
class CompanyAccessController {

    @Autowired
    lateinit var companyRepository: CompanyRepository

    @Autowired
    lateinit var companyAccessRepository: CompanyAccessRepository

    @Autowired
    lateinit var userRepository: UserRepository

//    @GetMapping("")
//    fun getCompanyAccesses(@PathVariable companyId: UUID): ResponseEntity<List<CompanyAccess>> {
//        val result = companyRepository.findById(companyId)
//        if (!result.isPresent) return ResponseEntity.notFound().build()
//
//        val accesses = result.get().companyAccesses.toList()
//        return ResponseEntity.ok(accesses)
//    }

    @PostMapping("")
    fun createCompanyAccess(
        @PathVariable companyId: UUID,
        @RequestBody companyAccess: CompanyAccess,
    ): ResponseEntity<String> {
        val result = companyRepository.findById(companyId)
        if (!result.isPresent) return ResponseEntity.notFound().build()

//        val badRequest = !Validator.validEmail(companyAccess.email)
//                || !Validator.validPassword(companyAccess.password)
//        if (badRequest) return ResponseEntity.badRequest().build()

        val companyAccess = companyAccess.copy(company = result.get())

        companyAccessRepository.save(companyAccess)
        return ResponseEntity("Company access created successfully", HttpStatus.CREATED)
    }

    @DeleteMapping("/{companyAccessId}")
    fun deleteCompanyAccess(@PathVariable companyAccessId: UUID) = companyAccessRepository.deleteById(companyAccessId)


    @GetMapping("")
    fun getAllUserCompanies(@RequestParam("userLogin") userLogin: String = ""): ResponseEntity<List<GetCompanyResponseDto>> {
        val user = userRepository.findById(userLogin)
        val result: List<Company> = if (user.isPresent) {
            companyAccessRepository.findByHeadUser(user.get()).map { it.company }
        } else {
            return ResponseEntity.badRequest().build()
        }

        val companies = result.map { company ->
            GetCompanyResponseDto(
                id = company.id.toString(),
                name = company.name,
                description = company.description,
                companySize = company.companySize,
                logoUrl = company.logoUrl,
            )
        }

        return ResponseEntity.ok(companies)
    }
}