package com.gethired.backend.security

import java.security.KeyFactory
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

object RSAReader {
    fun readPublicKey(resourceName: String): RSAPublicKey {
        val bytes = javaClass.getResourceAsStream(resourceName)?.readBytes()
        val keySpec = X509EncodedKeySpec(bytes)
        return KeyFactory.getInstance("RSA").generatePublic(keySpec) as RSAPublicKey
    }

    fun readPrivateKey(resourceName: String): RSAPrivateKey {
        val bytes = javaClass.getResourceAsStream(resourceName)?.readBytes()
        val keySpec = PKCS8EncodedKeySpec(bytes)
        return KeyFactory.getInstance("RSA").generatePrivate(keySpec) as RSAPrivateKey
    }
}