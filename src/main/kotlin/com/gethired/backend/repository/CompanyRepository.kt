package com.gethired.backend.repository

import com.gethired.backend.model.Company
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface CompanyRepository : JpaRepository<Company, UUID> {
    @Query("SELECT c FROM Company c WHERE c.name LIKE :name%")
    fun findByName(name: String): List<Company>
}