package com.gethired.backend.repository

import com.gethired.backend.model.Application
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface ApplicationRepository : JpaRepository<Application, UUID> {
    @Query("SELECT a FROM Application a WHERE a.user.username = :username")
    fun findByUserUsername(username: String): List<Application>

    @Query("SELECT a FROM Application a WHERE a.vacancy.company.id = :companyId")
    fun findAllByCompany(companyId: UUID): List<Application>
}