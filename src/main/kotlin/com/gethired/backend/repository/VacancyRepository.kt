package com.gethired.backend.repository

import com.gethired.backend.model.Vacancy
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface VacancyRepository : JpaRepository<Vacancy, UUID>