package com.gethired.backend.repository

import com.gethired.backend.model.CompanyAccess
import com.gethired.backend.model.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface CompanyAccessRepository : JpaRepository<CompanyAccess, UUID> {
    fun findByHeadUser(headUser: User): List<CompanyAccess>

}