package com.gethired.backend.repository

import com.gethired.backend.model.SkillTag
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface SkillTagRepository : JpaRepository<SkillTag, UUID>