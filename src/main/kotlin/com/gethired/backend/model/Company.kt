package com.gethired.backend.model

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "companies")
data class Company(

    @Id
    val id: UUID = UUID.randomUUID(),

    val name: String = "",

    val description: String = "",

    @Column(name = "company_size")
    val companySize: Int = 0,

    @Column(name = "logo_url")
    val logoUrl: String = "",

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    val companyAccesses: MutableSet<CompanyAccess> = mutableSetOf(),

)