package com.gethired.backend.dto.vacancy

data class GetVacanciesResponseDTO (
    val id: String,
    val title: String,
    val description: String,
    val companyName: String
)