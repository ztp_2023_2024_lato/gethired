package com.gethired.backend.dto.vacancy

data class CreateVacancyRequestDto(
    val title: String,
    val description: String,
    val companyId: String
)