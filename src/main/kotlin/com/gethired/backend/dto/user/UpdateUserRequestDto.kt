package com.gethired.backend.dto.user

data class UpdateUserRequestDto(
    val username: String? = null,
)