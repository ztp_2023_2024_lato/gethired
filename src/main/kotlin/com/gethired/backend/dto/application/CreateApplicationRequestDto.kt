package com.gethired.backend.dto.application

data class CreateApplicationRequestDto (
    val userId: String,
    val vacancyId: String,
    val text: String,
)