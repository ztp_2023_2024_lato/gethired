variable "project_name" {
  description = "The unique name of the project"
  type        = string
}

variable "environment" {
  description = "The deployment environment"
  type        = string
}

variable "region" {
  description = "The AWS region for resource deployment"
  default     = "eu-central-1"
  type        = string
}
