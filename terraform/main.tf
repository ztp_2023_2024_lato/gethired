module "ecr" {
  source       = "./modules/ecr"
  project_name = var.project_name
  environment  = var.environment
  region       = var.region
}

module "vpc" {
  source       = "./modules/vpc"
  project_name = var.project_name
  environment  = var.environment
}

module "rds" {
  source               = "./modules/rds"
  project_name         = var.project_name
  environment          = var.environment
  vpc                  = module.vpc.vpc
  sg                   = module.vpc.sg
  db_subnet_group_name = module.vpc.db_subnet_group_name
  depends_on           = [module.vpc]
}

module "iam" {
  source       = "./modules/iam"
  project_name = var.project_name
  environment  = var.environment
}

module "log" {
  source = "./modules/log"
}

module "alb" {
  source       = "./modules/alb"
  project_name = var.project_name
  environment  = var.environment
  vpc          = module.vpc.vpc
  sg           = module.vpc.sg
}

module "ecs" {
  source                  = "./modules/ecs"
  project_name            = var.project_name
  environment             = var.environment
  region                  = var.region
  sg                      = module.vpc.sg
  vpc                     = module.vpc.vpc
  tg                      = module.alb.tg
  lb_dns_name             = module.alb.lb_dns_name
  repository_url          = module.ecr.repository_url
  ecs_task_role           = module.iam.ecs_task_role
  ecs_task_execution_role = module.iam.ecs_task_execution_role
  log_group               = module.log.log_group
  db_address              = module.rds.db_address
  db_password             = module.rds.db_password
  depends_on              = [module.ecr, module.rds, module.iam]
}
