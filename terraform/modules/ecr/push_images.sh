#!/bin/bash
aws ecr get-login-password --region $PUSH_IMAGE_REGION | \
	docker login --username AWS --password-stdin $PUSH_IMAGE_REPOSITORY_URL

docker build --platform linux/amd64 -t $PUSH_IMAGE_REPOSITORY_URL:backend-latest ..
docker push $PUSH_IMAGE_REPOSITORY_URL:backend-latest

docker build --platform linux/amd64 -t $PUSH_IMAGE_REPOSITORY_URL:frontend-latest ../frontend
docker push $PUSH_IMAGE_REPOSITORY_URL:frontend-latest
