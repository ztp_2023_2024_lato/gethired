resource "aws_ecr_repository" "main" {
  name                 = join("-", [var.project_name, var.environment, "ecr"])
  image_tag_mutability = "MUTABLE"
  force_delete         = true
}

resource "null_resource" "push_images" {
  provisioner "local-exec" {
    command = <<COMMAND
	PUSH_IMAGE_REGION="${var.region}" \
	PUSH_IMAGE_REPOSITORY_URL="${aws_ecr_repository.main.repository_url}" \
	/bin/bash ${path.module}/push_images.sh
	COMMAND
  }
  depends_on = [aws_ecr_repository.main]
}
