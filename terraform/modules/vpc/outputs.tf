output "vpc" {
  value = module.vpc
}

output "sg" {
  value = {
    lb_id     = aws_security_group.lb_sg.id
    client_id = aws_security_group.client_sg.id
    server_id = aws_security_group.server_sg.id
    db_id     = aws_security_group.db_sg.id
  }
}

output "db_subnet_group_name" {
  value = aws_db_subnet_group.main.name
}
