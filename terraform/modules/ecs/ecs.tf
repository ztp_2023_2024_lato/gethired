resource "aws_ecs_cluster" "main" {
  name = join("-", [var.project_name, var.environment, "ecs"])
}

resource "aws_ecs_task_definition" "client" {
  family                = join("-", [var.project_name, var.environment, "td", "client"])
  container_definitions = <<DEFINITION
  [
    {
      "name": "${join("-", [var.project_name, var.environment, "container", "client"])}",
      "image": "${var.repository_url}:frontend-latest",
      "essential": true,
	  "networkMode": "awsvpc",
      "portMappings": [
        {
          "containerPort": 3000,
          "hostPort": 3000
        }
      ],
      "memory": 2048,
      "cpu": 1024,
	  "environment": [
        {
          "name": "REACT_APP_SERVER_DNS_URL",
          "value": "${var.lb_dns_name}:8080"
        }
	  ],
	  "logConfiguration": {
        "logDriver": "awslogs",
		"options": {
	  	  "awslogs-group": "${var.log_group}",
		  "awslogs-region": "${var.region}",
		  "awslogs-stream-prefix": "streaming"
		}
      }
    }
  ]
  DEFINITION

  requires_compatibilities = ["FARGATE"]
  runtime_platform {
    cpu_architecture        = "X86_64"
    operating_system_family = "LINUX"
  }

  network_mode       = "awsvpc"
  memory             = 2048
  cpu                = 1024
  task_role_arn      = var.ecs_task_role.arn
  execution_role_arn = var.ecs_task_execution_role.arn
}

resource "aws_ecs_task_definition" "server" {
  family                = join("-", [var.project_name, var.environment, "td", "server"])
  container_definitions = <<DEFINITION
  [
    {
      "name": "${join("-", [var.project_name, var.environment, "container", "server"])}",
      "image": "${var.repository_url}:backend-latest",
      "essential": true,
	  "networkMode": "awsvpc",
      "portMappings": [
        {
		  "name": "backend_port",
          "containerPort": 8080,
          "hostPort": 8080
        }
      ],
	  "environment": [
        {
          "name": "SPRING_PROFILE",
          "value": "cloud"
        },
        {
          "name": "DATABASE_URL",
          "value": "${var.db_address}"
        },
        {
          "name": "DATABASE_PASSWORD",
          "value": "${var.db_password}"
        }
	  ],
      "memory": 2048,
      "cpu": 1024,
	  "logConfiguration": {
        "logDriver": "awslogs",
		"options": {
	  	  "awslogs-group": "${var.log_group}",
		  "awslogs-region": "${var.region}",
		  "awslogs-stream-prefix": "streaming"
		}
      }
    }
  ]
  DEFINITION

  requires_compatibilities = ["FARGATE"]
  runtime_platform {
    cpu_architecture        = "X86_64"
    operating_system_family = "LINUX"
  }


  network_mode       = "awsvpc"
  memory             = 2048
  cpu                = 1024
  task_role_arn      = var.ecs_task_role.arn
  execution_role_arn = var.ecs_task_execution_role.arn
}

resource "aws_ecs_service" "client" {
  name            = join("-", [var.project_name, var.environment, "service", "client"])
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.client.arn
  launch_type     = "FARGATE"
  desired_count   = 1

  network_configuration {
    subnets          = var.vpc.public_subnets
    assign_public_ip = true
    security_groups  = [var.sg.client_id]
  }

  load_balancer {
    target_group_arn = var.tg.client.arn
    container_name   = join("-", [var.project_name, var.environment, "container", "client"])
    container_port   = 3000
  }
}

resource "aws_ecs_service" "server" {
  name            = join("-", [var.project_name, var.environment, "service", "server"])
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.server.arn
  launch_type     = "FARGATE"
  desired_count   = 1

  network_configuration {
    subnets          = var.vpc.public_subnets
    assign_public_ip = true
    security_groups  = [var.sg.server_id]
  }

  load_balancer {
    target_group_arn = var.tg.server.arn
    container_name   = join("-", [var.project_name, var.environment, "container", "server"])
    container_port   = 8080
  }
}
