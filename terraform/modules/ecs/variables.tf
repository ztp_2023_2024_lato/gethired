variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "region" {
  type = string
}

variable "repository_url" {
  type = string
}

variable "db_address" {
  type = string
}

variable "db_password" {
  type = string
}

variable "sg" {
  type = any
}

variable "vpc" {
  type = any
}

variable "tg" {
  type = any
}

variable "lb_dns_name" {
  type = string
}

variable "log_group" {
  type = any
}

variable "ecs_task_role" {
  type = any
}

variable "ecs_task_execution_role" {
  type = any
}
