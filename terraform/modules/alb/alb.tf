resource "aws_alb" "main" {
  name               = join("-", [var.project_name, var.environment, "alb"])
  internal           = false
  load_balancer_type = "application"
  subnets            = var.vpc.public_subnets
  security_groups    = [var.sg.lb_id]
}

resource "aws_alb_target_group" "client" {
  name        = join("-", [var.project_name, var.environment, "client", "tg"])
  port        = 3000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc.vpc_id
}

resource "aws_alb_target_group" "server" {
  name        = join("-", [var.project_name, var.environment, "server", "tg"])
  port        = 8080
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc.vpc_id
}

resource "aws_alb_listener" "client" {
  load_balancer_arn = aws_alb.main.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.client.arn
  }
}

resource "aws_alb_listener" "server" {
  load_balancer_arn = aws_alb.main.arn
  port              = 8080
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.server.arn
  }
}
