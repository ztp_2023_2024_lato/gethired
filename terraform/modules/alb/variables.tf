variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc" {
  type = any
}

variable "sg" {
  type = any
}
