output "tg" {
  value = {
    client = aws_alb_target_group.client
    server = aws_alb_target_group.server
  }
}

output "lb_dns_name" {
  value = aws_alb.main.dns_name
}
