resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "!#$%&*"
}

resource "aws_db_instance" "database" {
  allocated_storage      = 10
  engine                 = "postgres"
  engine_version         = "16.2"
  instance_class         = "db.t3.micro"
  identifier             = join("-", [var.project_name, var.environment, "postgres"])
  db_name                = "gethired"
  username               = "postgres"
  password               = random_password.password.result
  db_subnet_group_name   = var.db_subnet_group_name
  vpc_security_group_ids = [var.sg.db_id]
  skip_final_snapshot    = true
}
