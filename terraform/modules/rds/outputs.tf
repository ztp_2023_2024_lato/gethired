output "db_password" {
  value = random_password.password.result
}

output "db_address" {
  value = aws_db_instance.database.address
}
