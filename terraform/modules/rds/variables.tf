variable "project_name" {
  type = string
}

variable "environment" {
  type = string
}

variable "vpc" {
  type = any
}

variable "sg" {
  type = any
}

variable "db_subnet_group_name" {
  type = string
}
